from django import forms
TOPIC_CHOICES=(
	('general','general enquiry'),
	('bug','bug report'),
	('Suggestion','Suggestion'),
	)
class ContactForm(forms.Form):
	topic=forms.ChoiceField(choices=TOPIC_CHOICES)
	message=forms.CharField()
	sender=forms.EmailField()
	