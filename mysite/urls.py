from django.conf.urls import *
from mysite.views import current_datetime
from django.contrib import admin
from books.views import search,contact
# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
	url(r'^time/$', current_datetime),
	#url(r'^admin/', include(admin.site.urls)),
    url(r'^search/$',search),
    url(r'^contact/$',contact),
	
	#(\d{1,2})/ is passing a maximum of two dogit integer value in the url
)
    # Examples:
    # url(r'^$', 'mysite.views.home', name='home'),
    # url(r'^mysite/', include('mysite.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
   

